package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_job")
public class KJob {
    //作业ID
    @TableId(value = "job_id", type = IdType.AUTO)
    private Integer jobId;
    private Integer categoryId;
    //添加者
    private Integer addUser;
    //是否删除（1：存在；0：删除）
    @TableLogic(value = "1", delval = "0")
    private Integer delFlag;
    //编辑者
    private Integer editUser;
    //作业执行日志（外键ID）
    private Integer jobRecord;
    //定时策略（外键ID）
    private Integer jobQuartz;
    //作业的资源库ID
    private Integer jobRepositoryId;
    //状态（1：正在运行；2：已停止）
    private Integer jobStatus;
    //1:数据库资源库；2:上传的文件
    private Integer jobType;
    //任务描述
    private String jobDescription;
    //作业名称
    private String jobName;
    //作业保存路径（可以是资源库中的路径也可以是服务器中保存作业文件的路径）
    private String jobPath;
    //作业保存路径（可以是资源库中的路径也可以是服务器中保存作业文件的路径）
    private String jobTransPath;
    //日志级别(basic，detail，error，debug，minimal，rowlevel）
    private String jobLogLevel;
    //添加时间
    private Date addTime;
    //编辑时间
    private Date editTime;

}
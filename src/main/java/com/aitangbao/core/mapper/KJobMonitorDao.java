package com.aitangbao.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KJobMonitorDao extends BaseMapper<KJobMonitor> {

    List<KJobMonitor> pageQuery(@Param(value = "KJobMonitor") KJobMonitor template
            , @Param(value = "start") Integer start
            , @Param(value = "size") Integer size
            , @Param(value = "categoryId") Integer categoryId);

    Long allCount(@Param(value = "KJobMonitor") KJobMonitor template
            , @Param(value = "categoryId") Integer categoryId);
}
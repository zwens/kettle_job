package com.aitangbao.core.dto.kettle;

import lombok.Data;

@Data
public class RepositoryTree {

	private String id;
	private String parent;
	private String text;
	private String icon;
	private Object state;
	private String type;
	private boolean isLasted;
	private String path;
}
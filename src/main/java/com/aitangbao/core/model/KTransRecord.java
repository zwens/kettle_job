package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_trans_record")
public class KTransRecord {
	//转换记录ID
	@TableId(value = "record_id", type = IdType.AUTO)
	private Integer recordId ;
	//任务执行结果（1：成功；2：失败）
	private Integer recordStatus ;
	//转换ID
	private Integer recordTrans ;
	//添加人
	private Integer addUser;
	//转换日志记录文件保存位置
	private String logFilePath ;
	//启动时间
	private Date startTime ;
	//停止时间
	private Date stopTime ;
}
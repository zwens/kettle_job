package com.aitangbao.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.KJob;


public interface KJobDao extends BaseMapper<KJob> {
}
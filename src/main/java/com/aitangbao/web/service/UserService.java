package com.aitangbao.web.service;

import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aitangbao.common.toolkit.MD5Utils;
import com.aitangbao.core.dto.BootTablePage;
import com.aitangbao.core.mapper.KUserDao;
import com.aitangbao.core.model.KUser;

@Service
public class UserService {

	@Autowired
	private KUserDao kUserDao;

	/**
	 * @Title login
	 * @Description 登陆
	 * @param kUser 用户信息对象
	 * @return
	 * @return KUser
	 */
	public KUser login(KUser kUser){		
		QueryWrapper<KUser> kUserQueryWrapper = new QueryWrapper<>();
		kUserQueryWrapper.eq("u_account", kUser.getUAccount());
		KUser user = kUserDao.selectOne(kUserQueryWrapper);
		if (null != user){
			if (user.getUPassword().equals(MD5Utils.Encrypt(kUser.getUPassword(), true))){
				return user;
			}
			return null;
		}
		return null;
	}
	
	/**
	 * @Title isAdmin
	 * @Description 用户是否为管理员
	 * @param uId 用户ID
	 * @return
	 * @return boolean
	 */
	public boolean isAdmin(Integer uId){
		KUser kUser = kUserDao.selectById(uId);
		if ("admin".equals(kUser.getUAccount())){
			return true;
		}else {
			return false;	
		}
	}
	
	/**
	 * @Title getList
	 * @Description 获取用户分页列表
	 * @param start 其实行数
	 * @param size 每页显示行数
	 * @return 
	 * @return BootTablePage
	 */
	public BootTablePage getList(Integer start, Integer size){
		//根据当前数量求页数
		if (start != null && size !=null) {
			start = start/size + 1;
		}
		Page page = new Page(start, size);
		IPage<KUser> iPage = kUserDao.selectPage(page, new QueryWrapper<KUser>());
		BootTablePage bootTablePage = new BootTablePage();
		bootTablePage.setRows(iPage.getRecords());
		bootTablePage.setTotal(iPage.getTotal());
		return bootTablePage;
	}
	
	/**
	 * @Title delete
	 * @Description 删除用户
	 * @param uId 用户ID
	 * @return void
	 */
	public void delete(Integer uId){
		kUserDao.deleteById(uId);
	}
	
	/**
	 * @Title insert
	 * @Description 插入一个用户
	 * @param kUser
	 * @return void
	 */
	public void insert(KUser kUser, Integer uId){
		kUser.setUPassword(MD5Utils.Encrypt(kUser.getUPassword(), true));
		kUser.setAddTime(new Date());
		kUser.setAddUser(uId);
		kUser.setEditTime(new Date());
		kUser.setEditUser(uId);
		kUser.setDelFlag(1);
		kUserDao.insert(kUser);
	}
	/**
	 * @Title IsAccountExist
	 * @Description 判断账号是否存在
	 * @param uAccount
	 * @return void
	 */
	public boolean IsAccountExist(String uAccount) {
		QueryWrapper<KUser> kUserQueryWrapper = new QueryWrapper<>();
		kUserQueryWrapper.eq("u_account", uAccount);
		KUser user = kUserDao.selectOne(kUserQueryWrapper);
		if (null == user) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @Title getUser
	 * @Description 获取 用户
	 * @param uId 用户ID
	 * @return
	 * @return KUser
	 */
	public KUser getUser(Integer uId){
		return kUserDao.selectById(uId);
	}
	/**
	 * @Title update
	 * @Description 更新用户
	 * @param kUser 用户对象
	 * @param uId 用户ID
	 * @return void
	 */
	public void update(KUser kUser, Integer uId){
		kUser.setEditTime(new Date());
		kUser.setEditUser(uId);
		//只有不为null的字段才参与更新
		kUserDao.updateById(kUser);
	}
}
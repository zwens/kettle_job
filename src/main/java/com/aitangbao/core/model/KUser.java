package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName(value = "k_user")
@Data
public class KUser {
	//用户ID
	@TableId(value = "u_id", type = IdType.AUTO)
	private Integer uId ;
	//添加者
	private Integer addUser ;
	//是否删除（1：存在；0：删除）
	@TableLogic(value = "1", delval = "0")
	private Integer delFlag ;
	//编辑者
	private Integer editUser ;
	//用户账号
	private String uAccount ;
	//用户邮箱
	private String uEmail ;
	//用户昵称
	private String uNickname ;
	//用户密码
	private String uPassword ;
	//用于电话
	private String uPhone ;
	//添加时间
	private Date addTime ;
	//编辑时间
	private Date editTime ;

}
package com.aitangbao.core.model;

import lombok.Data;

@Data
public class KRepositoryType {
	private Integer repositoryTypeId ;
	private String repositoryTypeCode ;
	private String repositoryTypeDes ;
	
}
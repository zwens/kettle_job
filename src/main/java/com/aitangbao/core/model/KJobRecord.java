package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_job_record")
public class KJobRecord {
	//作业记录ID
	@TableId(value = "record_id", type = IdType.AUTO)
	private Integer recordId ;
	//作业ID
	private Integer recordJob ;
	//任务执行结果（1：成功；2：失败）
	private Integer recordStatus ;
	//添加人
	private Integer addUser;
	//作业日志记录文件保存位置
	private String logFilePath ;
	//启动时间
	private Date startTime ;
	//停止时间
	private Date stopTime ;

}
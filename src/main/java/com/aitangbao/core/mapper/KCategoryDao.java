package com.aitangbao.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.KCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface KCategoryDao extends BaseMapper<KCategory> {
}

package com.aitangbao.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KTransMonitorDao extends BaseMapper<KTransMonitor> {

    List<KTransMonitor> pageQuery(@Param(value = "KTransMonitor") KTransMonitor template
            , @Param(value = "start") Integer start
            , @Param(value = "size") Integer size
            , @Param(value = "categoryId") Integer categoryId);


    Long allCount(@Param(value = "KTransMonitor") KTransMonitor template,@Param(value = "categoryId") Integer categoryId);
}
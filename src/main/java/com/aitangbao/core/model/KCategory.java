package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_category")
public class KCategory {
    //分类ID
    @TableId(value = "category_id", type = IdType.AUTO)
    private Integer categoryId ;
    //分类名称
    private String categoryName;
    //添加者
    private Integer addUser ;
    //是否删除（1：存在；0：删除）
    @TableLogic(value = "1", delval = "0")
    private Integer delFlag ;
    //编辑者
    private Integer editUser ;
    //添加时间
    private Date addTime ;
    //编辑时间
    private Date editTime ;


}

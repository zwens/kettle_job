package com.aitangbao.core.model;

import lombok.Data;

/**
 * 定义保存job文件返回值
 */
@Data
public class KJobTrans {

    private String jobPath;  //作业路径

    private String jobTransPath; //关联同目录下转换名称

}

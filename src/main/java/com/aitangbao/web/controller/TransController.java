package com.aitangbao.web.controller;

import java.io.*;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aitangbao.common.toolkit.Constant;
import com.aitangbao.core.dto.BootTablePage;
import com.aitangbao.core.dto.ResultDto;
import com.aitangbao.core.model.KTrans;
import com.aitangbao.core.model.KUser;
import com.aitangbao.web.service.TransService;
import com.aitangbao.web.utils.JsonUtils;

@RestController
@RequestMapping("/trans/")
@Slf4j
public class TransController {

    @Autowired
    private TransService transService;


    @RequestMapping("getSimpleList.shtml")
    public String getSimpleList(HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        return JsonUtils.objectToJson(transService.getList(kUser.getUId()));
    }

    @RequestMapping("getList.shtml")
    public String getList(Integer offset, Integer limit, Integer categoryId, String transName, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        BootTablePage list = transService.getList(offset, limit, categoryId, transName, kUser.getUId());
        return JsonUtils.objectToJson(list);
    }

    @RequestMapping("delete.shtml")
    public String delete(Integer transId) {
        transService.delete(transId);
        return ResultDto.success();
    }

    @RequestMapping("uploadTrans.shtml")
    public String uploadJob(MultipartFile transFile, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        try {
            String saveFilePath = transService.saveFile(kUser.getUId(), transFile);
            return ResultDto.success(saveFilePath);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping("insert.shtml")
    public String insert(KTrans kTrans, String customerQuarz, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        Integer uId = kUser.getUId();
        if (transService.check(kTrans.getTransRepositoryId(), kTrans.getTransPath(), uId)) {
            try {
                transService.insert(kTrans, uId, customerQuarz);
                return ResultDto.success();
            } catch (SQLException e) {
                e.printStackTrace();
                return ResultDto.fail("添加作业失败");
            }
        } else {
            return ResultDto.fail("该作业已经添加过了");
        }
    }

    @RequestMapping("getTrans.shtml")
    public String getTrans(Integer transId) {
        return ResultDto.success(transService.getTrans(transId));
    }

    @RequestMapping("update.shtml")
    public String update(KTrans kTrans, String customerQuarz, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        try {
            transService.update(kTrans, customerQuarz, kUser.getUId());
            return ResultDto.success();
        } catch (Exception e) {
            return ResultDto.success(e.toString());
        }
    }

    @RequestMapping("start.shtml")
    public String start(Integer transId) {
        transService.start(transId);
        return ResultDto.success();
    }

    @RequestMapping("stop.shtml")
    public String stop(Integer transId) {
        transService.stop(transId);
        return ResultDto.success();
    }

    @RequestMapping("startAll.shtml")
    public String startAll(Integer categoryId, String transName, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        transService.startAll(categoryId, transName, kUser.getUId());
        return ResultDto.success();
    }

    @RequestMapping("stopAll.shtml")
    public String stopAll(Integer categoryId, String transName, HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        transService.stopAll(categoryId, transName, kUser.getUId());
        return ResultDto.success();
    }

    @RequestMapping("getStartTaskCount.shtml")
    public String getStartTaskCount(Integer categoryId, String transName,HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        return JsonUtils.objectToJson(transService.getStartTaskCount(categoryId, transName, kUser.getUId()));
    }

    @RequestMapping("getStopTaskCount.shtml")
    public String getStopTaskCount(Integer categoryId, String transName,HttpServletRequest request) {
        KUser kUser = (KUser) request.getSession().getAttribute(Constant.SESSION_ID);
        return JsonUtils.objectToJson(transService.getStopTaskCount(categoryId, transName, kUser.getUId()));
    }
    @RequestMapping({"getTransRunState.shtml"})
    public String getTransRunState(Integer transId) {
        return JsonUtils.objectToJson(this.transService.getTransRunState(transId));
    }


    /**
     * 判断作业是否存在根据jobid
     *
     * @param transId
     * @return
     */
    @RequestMapping(value = {"judgeHaveTransFile.shtml"}, method = RequestMethod.GET)
    @ResponseBody
    public String judgeHaveTransFile(Integer transId) {
        try {
            KTrans trans = transService.getTrans(transId);
            String transPath = trans.getTransPath();
            File file = new File(transPath);
            if (!FileUtil.exist(file)) {
                return ResultDto.fail("转换文件丢失！");
            }
        } catch (Exception e) {
            log.error("下载转换异常", e);
            return ResultDto.fail("下载转换异常");
        }
        return ResultDto.success();
    }

    @RequestMapping({"downTransFile.shtml"})
    public void downTransFile(Integer transId, HttpServletResponse response){
        KTrans trans = transService.getTrans(transId);
        // path是指欲下载的文件的路径。
        String path = trans.getTransPath();
        InputStream fis;
        OutputStream toClient;
        try {
            File file = new File(path);
            //获取原先文件名
            String orgName = FileUtil.getName(file);
            // 以流的形式下载文件。
            fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + orgName);
            response.addHeader("Content-Length", "" + file.length());
            toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

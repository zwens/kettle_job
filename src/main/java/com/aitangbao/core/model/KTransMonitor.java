package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_trans_monitor")
public class KTransMonitor implements Comparable<KTransMonitor> {
    //监控转换ID
    @TableId(value = "monitor_id", type = IdType.AUTO)
    private Integer monitorId;
    //添加人
    private Integer addUser;
    //失败次数
    private Integer monitorFail;
    //监控状态（是否启动，1:启动；2:停止）
    private Integer monitorStatus;
    //成功次数
    private Integer monitorSuccess;
    //监控的转换的ID
    private Integer monitorTrans;
    //运行状态（起始时间-结束时间,起始时间-结束时间……）
    private String runStatus;
    //上次执行时间
    private Date lastExecuteTime;
    //下次执行时间
    private Date nextExecuteTime;
    @TableField(exist = false)
    private String categoryName;
    @TableField(exist = false)
    private String transName;

    @Override
    public int compareTo(KTransMonitor o) {
        return this.getMonitorSuccess() - o.getMonitorSuccess();
    }

}
package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_trans")
public class KTrans {
    //转换ID
    @TableId(value = "trans_id", type = IdType.AUTO)
    private Integer transId;
    private Integer categoryId;
    //添加者
    private Integer addUser;
    //是否删除（1：存在；0：删除）
    @TableLogic(value = "1", delval = "0")
    private Integer delFlag;
    //编辑者
    private Integer editUser;
    //定时策略（外键ID）
    private Integer transQuartz;
    //转换执行记录（外键ID）
    private Integer transRecord;
    //转换的资源库ID
    private Integer transRepositoryId;
    //状态（1：正在运行；2：已停止）
    private Integer transStatus;
    //1:数据库资源库；2:上传的文件
    private Integer transType;
    //转换描述
    private String transDescription;
    //日志级别(basic，detail，error，debug，minimal，rowlevel）
    private String transLogLevel;
    //转换名称
    private String transName;
    //转换保存路径（可以是资源库中的路径也可以是服务器中保存作业文件的路径）
    private String transPath;
    //添加时间
    private Date addTime;
    //编辑时间
    private Date editTime;
}
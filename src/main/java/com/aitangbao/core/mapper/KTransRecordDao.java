package com.aitangbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.*;


public interface KTransRecordDao extends BaseMapper<KTransRecord> {
	
}
package com.aitangbao.web.service;

import java.io.File;
import java.io.IOException;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aitangbao.common.toolkit.Constant;
import com.aitangbao.core.dto.BootTablePage;
import com.aitangbao.core.mapper.KTransRecordDao;
import com.aitangbao.core.model.KTransRecord;

@Service
public class TransRecordService {

	@Autowired
	private KTransRecordDao kTransRecordDao;
	
	/**
	 * @Title getList
	 * @Description 获取列表
	 * @param start 其实行数
	 * @param size 结束行数
	 * @param uId 用户ID
	 * @param transId 转换ID
	 * @return
	 * @return BootTablePage
	 */
	public BootTablePage getList(Integer start, Integer size, Integer uId, Integer transId){
		Page<KTransRecord> page = new Page(start, size);
		QueryWrapper<KTransRecord> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("add_user", uId);
		if (transId != null){
			queryWrapper.eq("record_trans", transId);
		}
		IPage<KTransRecord> iPage = kTransRecordDao.selectPage(page, queryWrapper);
		BootTablePage bootTablePage = new BootTablePage();
		bootTablePage.setRows(iPage.getRecords());
		bootTablePage.setTotal(iPage.getTotal());
		return bootTablePage;
	}
	
	/**
	 * @Title getLogContent
	 * @Description 转换日志内容
	 * @param recordId 转换记录ID
	 * @return
	 * @throws IOException
	 * @return String
	 */
	public String getLogContent(Integer recordId) throws IOException{
		KTransRecord kTransRecord = kTransRecordDao.selectById(recordId);
		String logFilePath = kTransRecord.getLogFilePath();
		return FileUtils.readFileToString(new File(logFilePath), Constant.DEFAULT_ENCODING);
	}
	
}
package com.aitangbao.web.service;

import java.io.File;
import java.io.IOException;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aitangbao.common.toolkit.Constant;
import com.aitangbao.core.dto.BootTablePage;
import com.aitangbao.core.mapper.KJobRecordDao;
import com.aitangbao.core.model.KJobRecord;

@Service
public class JobRecordService {

	@Autowired
	private KJobRecordDao kJobRecordDao;
	
	/**
	 * @Title getList
	 * @Description 获取带分页的列表
	 * @param start 起始行数
	 * @param size 每页行数
	 * @param uId 用户ID
	 * @param jobId 作业ID
	 * @return
	 * @return BootTablePage
	 */
	public BootTablePage getList(Integer start, Integer size, Integer uId, Integer jobId){
		//根据当前数量求页数
		if (start != null && size !=null) {
			start = start/size + 1;
		}
		Page<KJobRecord> page = new Page(start, size);
		QueryWrapper<KJobRecord> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("add_user", uId);
		if (jobId != null){
			queryWrapper.eq("record_job", jobId);
		}
		IPage<KJobRecord> iPage = kJobRecordDao.selectPage(page, queryWrapper);
		BootTablePage bootTablePage = new BootTablePage();
		bootTablePage.setRows(iPage.getRecords());
		bootTablePage.setTotal(iPage.getTotal());
		return bootTablePage;
	}
	
	/**
	 * @Title getLogContent
	 * @Description 转换日志内容
	 * @return
	 * @throws IOException
	 * @return String
	 */
	public String getLogContent(Integer jobId) throws IOException{
		KJobRecord kJobRecord = kJobRecordDao.selectById(jobId);
		String logFilePath = kJobRecord.getLogFilePath();
		return FileUtils.readFileToString(new File(logFilePath), Constant.DEFAULT_ENCODING);
	}
}
package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_quartz")
public class KQuartz {
	//任务ID
	@TableId(value = "quartz_id", type = IdType.AUTO)
	private Integer quartzId ;
	//添加者
	private Integer addUser ;
	//是否删除（1：存在；0：删除）
	@TableLogic(value = "1", delval = "0")
	private Integer delFlag ;
	//编辑者
	private Integer editUser ;
	//定时策略
	private String quartzCron ;
	//任务描述
	private String quartzDescription ;
	//添加时间
	private Date addTime ;
	//编辑时间
	private Date editTime ;

	
}
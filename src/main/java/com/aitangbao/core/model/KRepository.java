package com.aitangbao.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "k_repository")
public class KRepository {
	//ID
	@TableId(value = "repository_id", type = IdType.AUTO)
	private Integer repositoryId ;
	//添加者
	private Integer addUser ;
	//是否删除（1：存在；0：删除）
	@TableLogic(value = "1", delval = "0")
	private Integer delFlag ;
	//编辑者
	private Integer editUser ;
	//资源库数据库访问模式（"Native", "ODBC", "OCI", "Plugin", "JNDI")
	private String databaseAccess ;
	//资源库数据库主机名或者IP地址
	private String databaseHost ;
	//资源库数据库名称
	private String databaseName ;
	//数据库登录密码
	private String databasePassword ;
	//资源库数据库端口号
	private String databasePort ;
	//数据库登录账号
	private String databaseUsername ;
	//资源库名称
	private String repositoryName ;
	//登录密码
	private String repositoryPassword ;
	//资源库数据库类型（MYSQL、ORACLE）
	private String repositoryType ;
	//登录用户名
	private String repositoryUsername ;
	//添加时间
	private Date addTime ;
	//编辑时间
	private Date editTime ;
}
package com.aitangbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.aitangbao.core.model.*;
import org.springframework.stereotype.Repository;

@Repository
public interface KUserDao extends BaseMapper<KUser> {
	
}